/* See LICENSE file for copyright and license details. */

/* appearance */
static unsigned int borderpx  = 3;        /* border pixel of windows */
static unsigned int gappx     = 3;        /* gaps between windows */
static unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 1;        /* 1 means swallow floating windows by default */
static int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static char font[]          = { "monospace:size=13" };
static char dmenufont[]       = { "monospace:size=13" };
static const char *fonts[]          = { font };
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#f7ca88";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
	/*               fg           bg           border   */
	[SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
	[SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
 };
static const XPoint stickyicon[]    = { {0,0}, {4,0}, {4,8}, {2,6}, {0,8}, {0,0} }; /* represents the icon as an array of vertices */
static const XPoint stickyiconbb    = {4,8};	/* defines the bottom right corner of the polygon's bounding box (speeds up scaling) */

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Firefox", NULL,     NULL,           1 << 8,    0,          0,          -1,        -1 },
	{ "St",      NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *dmenucmd[] = {
	"dmenu_run",
	"-fn", dmenufont,
	"-nb", normbgcolor,
	"-nf", normfgcolor,
	"-sb", selbgcolor,
	"-sf", selfgcolor,
	topbar ? NULL : "-b",
	NULL
};
static const char *termcmd[]  = { "st", NULL };
static const char *seltermcmd[]    = { "sd &", NULL };
static const char *browsercmd[]    = { "qutebrowser", NULL };
static const char *torbrowsercmd[] = { "torbrowser-launcher", NULL };
static const char *graphxcmd[]     = { "gimp", NULL };
static const char *selgraphxcmd[]  = { "darktable", NULL };

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
	{ "background",        	STRING,  &normbgcolor },
	{ "color8",         	STRING,  &selbgcolor },
	{ "foreground",        	STRING,  &normfgcolor },
	{ "color7",         	STRING,  &selfgcolor },
	{ "color0",    		    STRING,  &normbordercolor },
	{ "color3",     	    STRING,  &selbordercolor },
	{ "font",               STRING,  &font },
	{ "dmenufont",          STRING,  &dmenufont },
	{ "borderpx",          	INTEGER, &borderpx },
	{ "gappx",          	INTEGER, &gappx },
	{ "snap",          	    INTEGER, &snap },
	{ "showbar",          	INTEGER, &showbar },
	{ "nmaster",          	INTEGER, &nmaster },
	{ "resizehints",       	INTEGER, &resizehints },
	{ "mfact",      	    FLOAT,   &mfact },
};

#include <X11/XF86keysym.h>
static const Key keys[] = {
	/* modifier                     key        function        argument */

	/* Commands */
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	/* { MODKEY,                       XK_e,      spawn,          {.v = emacscmd } }, */
	{ MODKEY,                       XK_e,      spawn,          SHCMD("emacsclient -c -a 'emacs'") },
	{ MODKEY|ControlMask,           XK_Return,     spawn,      {.v = seltermcmd } },
    { MODKEY,                       XK_s,          spawn,      {.v = browsercmd} },
    { MODKEY,			            XK_o,          spawn,      {.v = torbrowsercmd} },
    { MODKEY,			            XK_g,          spawn,      {.v = graphxcmd} },
    { MODKEY|ShiftMask,             XK_g,          spawn,      {.v = selgraphxcmd} },

	/* Scripts */
	{ MODKEY,                       XK_backslash,  spawn,                  SHCMD("xdotool type $(grep -v '^#' ~/.local/share/snippets | dmenu -i -l 50 | cut -d' ' -f1)") },
	{ MODKEY|ShiftMask,             XK_backslash,  spawn,                  SHCMD("snippethis") },
    { MODKEY|ShiftMask,             XK_m,          spawn,                  SHCMD("mw -Y") },
    { MODKEY,                       XK_q,          spawn,                  SHCMD("dm-elogind") },
    { MODKEY,                       XK_c,          spawn,                  SHCMD("capture") },
    { MODKEY,                       XK_m,          spawn,                  SHCMD("dm-mount") },
    { MODKEY,                       XK_u,          spawn,                  SHCMD("dm-umount") },
    { MODKEY|ShiftMask,             XK_e,          spawn,                  SHCMD("dm-unicode") },
    { MODKEY|ShiftMask,             XK_t,          spawn,                  SHCMD("dm-kill") },
    { MODKEY,                       XK_x,          spawn,                  SHCMD("xw") },
    { MODKEY,                       XK_v,          spawn,                  SHCMD("~/.local/bin/mpv-handler") },
    { MODKEY,                       XK_p,          spawn,                  SHCMD("passmenu") },
    { MODKEY|ShiftMask,             XK_e,          spawn,                  SHCMD("setxkbmap -layout us && remaps") },
    { MODKEY|ShiftMask,             XK_i,          spawn,                  SHCMD("setxkbmap -layout us -variant intl && remaps") },
    { MODKEY|ShiftMask,             XK_b,          spawn,                  SHCMD("setxkbmap -layout br && remaps") },
    { MODKEY,                       XK_minus,      spawn,                  SHCMD("pamixer --allow-boost -d 5") },
    { MODKEY|ShiftMask,             XK_minus,      spawn,                  SHCMD("pamixer --allow-boost -d 15") },
    { 0,                            XF86XK_AudioLowerVolume, spawn,        SHCMD("pamixer --allow-boost -d 3") },
    { MODKEY,                       XK_equal,      spawn,                  SHCMD("pamixer --allow-boost -i 5") },
    { MODKEY|ShiftMask,             XK_equal,      spawn,                  SHCMD("pamixer --allow-boost -i 15") },
    { 0,                            XF86XK_AudioRaiseVolume, spawn,        SHCMD("pamixer --allow-boost -i 3") },
    { MODKEY,                       XK_BackSpace,  spawn,                  SHCMD("pamixer -t") },
    { MODKEY|ShiftMask,             XK_BackSpace,  spawn,                  SHCMD("pamixer -m") },
    { 0,                            XF86XK_AudioMute,spawn,                SHCMD("pamixer -t") },
    { MODKEY|ControlMask,           XK_minus,      spawn,                  SHCMD("xbacklight -dec 5") },
    { MODKEY|ControlMask|ShiftMask, XK_minus,      spawn,                  SHCMD("xbacklight -set 0") },
    { 0,                            XF86XK_AudioLowerVolume, spawn,        SHCMD("xbacklight -dec 5") },
    { MODKEY|ControlMask,           XK_equal,      spawn,                  SHCMD("xbacklight -inc 5") },
    { MODKEY|ControlMask|ShiftMask, XK_equal,      spawn,                  SHCMD("xbacklight -set 100") },
    { 0,                            XF86XK_AudioRaiseVolume, spawn,        SHCMD("xbacklight -inc 5") },

	/* Windows */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_space,  focusmaster,    {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_space,  zoom,           {0} },
	{ MODKEY|ShiftMask,             XK_j,      pushdown,       {0} },
	{ MODKEY|ShiftMask,             XK_k,      pushup,         {0} },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_y,      fullscreen,     {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },

	/* Layouts */
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_y,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_Tab,    setlayout,      {0} },
	{ MODKEY|ControlMask,           XK_space,  togglefloating, {0} },

	/* Monitor */
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

	/* Tags */
	{ MODKEY|ShiftMask,             XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

